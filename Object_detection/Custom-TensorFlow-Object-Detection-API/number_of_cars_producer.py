"""
this file is an implementation for RabbitMQ message broker architecture.
we are using Pika which is the Python client recommended by the RabbitMQ.
"""
import logging
import json
import pika

logging.basicConfig()


def params_to_json(identifier, cars_left, cars_right):
    """
    dump the entered params into a json file
    :param identifier: a message identifier
    :param cars_left: an integer
    :param cars_right: an integer
    :return: json file
    """
    data = {
        "id": identifier,
        "cars_left": cars_left,
        "cars_right": cars_right
    }
    message = json.dumps(data)
    return message


def connect_to_host(host):
    """
    connect to the hosted machine
    :param host: ip adress
    :return: the connection params
    """
    host_str = str(host)
    connection = pika.BlockingConnection(pika.ConnectionParameters(host_str))
    channel = connection.channel()
    return channel, connection


def send_message_to_queue(message, channel, connection, queue):
    """
    Send a message to the specific queue
    :param message: a dumped json file
    :param channel:
    :param connection:
    :param queue: string
    :return: None
    """
    # create a queue
    queue_str = str(queue)
    channel.queue_declare(queue=queue_str)
    # send a message
    channel.basic_publish(exchange='',
                          routing_key=queue_str,
                          body=message)
    print("[*] the traffic params were successfully sent !")
    # close the connection
    connection.close()
