"""
this is a test file for generate tfrecord
"""
import generate_tfrecord
import pytest
import pandas as pd
import os


@pytest.mark.parametrize('vehicle_type, number',
                         [
                             ('articulated_truck', 1),
                             ('bicycle', 2),
                             ('bus', 3),
                             ('car', 4),
                             ('motorcycle', 5),
                             ('motorized_vehicle', 6),
                             ('non-motorized_vehicle', 7),
                             ('pedestrian', 8),
                             ('pickup_truck', 9),
                             ('single_unit_truck', 10),
                             ('work_van', 11)
                         ]

                         )
def test_class_text_to_int(vehicle_type, number):
    """
    this is a test for text_to_int() function
    :param vehicle_type: all types of vehicle according to the label map
    :param number: the identifier of each vehicle type
    """
    assert generate_tfrecord.class_text_to_int(vehicle_type) == number


def test_correction():
    """
    this is a test for correction() function
    """
    assert generate_tfrecord.correction(1) == '00000001'
    assert generate_tfrecord.correction(2) == '00000002'
    assert generate_tfrecord.correction(10) == '00000010'



"""
def test_create_tf_example():
    
    this is a test for create_tf_record() function
    
    csv = 'pics/test_labels.csv'
    data = pd.read_csv(csv)
    path = os.path.join('images')
    arrays = generate_tfrecord.split(data, 'filename')
    file = open("pics/test_file.txt", "r")
    string = file.read()
    for array in arrays:
        assert str(generate_tfrecord.create_tf_example(array, path)) == string

"""