from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from abc import ABCMeta
from abc import abstractmethod
import six

class DataDecoder(six.with_metaclass(ABCMeta, object)):
  """Interface for data decoders."""

  @abstractmethod
  def decode(self, data):

    pass
