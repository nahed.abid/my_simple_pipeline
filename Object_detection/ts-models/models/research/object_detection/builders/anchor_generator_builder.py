from object_detection.anchor_generators import flexible_grid_anchor_generator
from object_detection.anchor_generators import grid_anchor_generator
from object_detection.anchor_generators import multiple_grid_anchor_generator
from object_detection.anchor_generators import multiscale_grid_anchor_generator
from object_detection.protos import anchor_generator_pb2


def build(anchor_generator_config):
  """Builds an anchor generator based on the config.

  Args:
    anchor_generator_config: An anchor_generator.proto object containing the
      config for the desired anchor generator.

  Returns:
    Anchor generator based on the config.

  Raises:
    ValueError: On empty anchor generator proto.
  """
  if not isinstance(anchor_generator_config,
                    anchor_generator_pb2.AnchorGenerator):
    raise ValueError('anchor_generator_config not of type '
                     'anchor_generator_pb2.AnchorGenerator')
  if anchor_generator_config.WhichOneof(
      'anchor_generator_oneof') == 'grid_anchor_generator':
    grid_anchor_generator_config = anchor_generator_config.grid_anchor_generator
    return grid_anchor_generator.GridAnchorGenerator(
        scales=[float(scale) for scale in grid_anchor_generator_config.scales],
        aspect_ratios=[float(aspect_ratio)
                       for aspect_ratio
                       in grid_anchor_generator_config.aspect_ratios],
        base_anchor_size=[grid_anchor_generator_config.height,
                          grid_anchor_generator_config.width],
        anchor_stride=[grid_anchor_generator_config.height_stride,
                       grid_anchor_generator_config.width_stride],
        anchor_offset=[grid_anchor_generator_config.height_offset,
                       grid_anchor_generator_config.width_offset])
  elif anchor_generator_config.WhichOneof(
      'anchor_generator_oneof') == 'multiscale_anchor_generator':
    cfg = anchor_generator_config.multiscale_anchor_generator
    return multiscale_grid_anchor_generator.MultiscaleGridAnchorGenerator(
        cfg.min_level,
        cfg.max_level,
        cfg.anchor_scale,
        [float(aspect_ratio) for aspect_ratio in cfg.aspect_ratios],
        cfg.scales_per_octave,
        cfg.normalize_coordinates
    )
  elif anchor_generator_config.WhichOneof(
      'anchor_generator_oneof') == 'flexible_grid_anchor_generator':
    cfg = anchor_generator_config.flexible_grid_anchor_generator
    base_sizes = []
    aspect_ratios = []
    strides = []
    offsets = []
    for anchor_grid in cfg.anchor_grid:
      base_sizes.append(tuple(anchor_grid.base_sizes))
      aspect_ratios.append(tuple(anchor_grid.aspect_ratios))
      strides.append((anchor_grid.height_stride, anchor_grid.width_stride))
      offsets.append((anchor_grid.height_offset, anchor_grid.width_offset))
    return flexible_grid_anchor_generator.FlexibleGridAnchorGenerator(
        base_sizes, aspect_ratios, strides, offsets, cfg.normalize_coordinates)
  else:
    raise ValueError('Empty anchor generator.')
