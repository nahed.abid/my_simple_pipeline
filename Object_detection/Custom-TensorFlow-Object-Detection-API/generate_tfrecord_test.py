"""
This is a test file for generate_tfrecord.py by unittest
"""
from object_detection import tfrecord
import unittest
from parameterized import parameterized
import pandas as pd
import os


class TestSequence(unittest.TestCase):

    @parameterized.expand([
        ["car", 1],
        ["truck", 2],
        ["vehicule", 3],
    ])
    def test_sequence(self, Label,LabelRow):
        """this is a test for class_text_to_int function
           :param Label: all types of vehicle according to the label map
           :param LabelRow: the identifier of each vehicle type"""
        self.assertIs(tfrecord.class_text_to_int(Label), LabelRow)


    def test_correction(self):
        """
        this is a test for correction function
        """
        self.assertTrue(tfrecord.correction(1)=="00000001")
        self.assertTrue(tfrecord.correction(2)=="00000002")
        self.assertTrue(tfrecord.correction(10)=="00000010")

    """def test_split(self):
       
        #this is a test for split function
      

        csv = 'pics/test_labels.csv'
        data = pd.read_csv(csv)
        result = "[data(filename='frame104', object=   filename  width  height       class  xmin  ymin  xmax  ymax\n0  frame104   1280     720  motorcycle   654   154   712   262)]"
        self.assertEqual(str(generate_tfrecord.split(data, "filename")), result)
"""


if __name__ == '__main__':
    unittest.main()

