# Smart Traffic Lights

## Summary
This project is the first phase of the realization of a global automatic system for the monitoring and control of road traffic.
This readme describes every step required to run the project:
1. Download this repository from Gitlab
2. Install Docker
3. Build Dockerfile
4. Run Docker compose
5. Test the vehicles detection model

## Introduction
This is the first phase of the realization of a global automatic system for the control of road traffic, the system will be able to:
- Detect the presence of vehicles of each lane and give their number in real time.
- Provide statistics hour by hour on traffic status
- Update information on CO2 level in real time
- Adjust the cycle automatically to give more time to the green light which favors the fluidity of the traffic

This repository is written for Windows 10, and it will also work for Windows 7 and 8.
The general procedure can also be used for Linux operating systems.

## Steps
### 1. Download this repository from Gitlab
you can simply download the stl repository or in the command terminal clone it by issuing the following command:
```
C:\> git clone -b dev http://gitlab.tools.sofia.local/sofia/smart-traffic-lights/stl.git
```
At this point, here is what your folder should look like:
<p align="center">
  <img src="readme_pic/readme1.PNG">
</p>

### 2. Install Docker
#### 2a. Install Docker and Docker compose for Windows
Install Docker and Docker compose for Windows from https://runnable.com/docker/install-docker-on-windows-10

#### 2b. Install Docker and Docker compose for Linux
Install Docker and Docker compose for Windows from https://runnable.com/docker/install-docker-on-linux

#### 2c.  Install Docker and Docker compose for MacOs
Install Docker and Docker compose for Windows from https://runnable.com/docker/install-docker-on-macos

### 3. Build Dockerfile
First make sure you are under the stl folder then run the following command :
```
C:\stl> docker-compose build
```
And you’ll see the process:
<p align="center">
  <img src="readme_pic/readme2.PNG">
</p>

### 4. Run Docker compose
To run containers from the Docker compose use command:
```
C:\stl> docker-compose up -d
```
### 5. Test the vehicles detection model
If everything went successfuly you should see the following result on your browser open  http://localhost:8888/ and use root as password:
<p align="center">
  <img src="readme_pic/readme3.png">
</p>

Open Object_detection_image.py and execute it.
<p align="center">
  <img src="readme_pic/readme4.PNG">
</p>