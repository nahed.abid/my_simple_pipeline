# STL 

## Python unittest 

Python unittest module is used to test a unit of source code. Suppose, you need to test your project.
You know what kind of data the function will return. After writing huge code, you need to check it whether the output is correct or not.
<p align="center">
	<img src="read-me\pic_readme.PNG"
</p>

### installing 

To install the unittest you can use this instruction on prompt :
```bash
pip install unittest2
```

### Python Unit Test Example 

First of all we have to write some code to unit test them. We will have a Python class.
a simple exemple 
```python 
def addition(a,b):
    return a+b
```
###Python unittest structure

Now, let’s learn how to code for unit testing. An individual testcase is created by subclassing unittest.TestCase. 
By overriding or adding appropriate functions, we can add logic to test. The following code will be succeeded if a is equals to b.

```pytnon 

import unittest
import module

class MyTestCase(unittest.TestCase):

    def test_addition(self):
        a=2
        b=3
        self.assertEqual(module.addition(a,b), a+b)


if __name__ == '__main__':
    unittest.main()
```

you can use command prompt to run this module. For example, we named the file for unit-testing as module_Test.py. 
So the command to run python unittest will be:
```python 
python3 -m unittest module_Test
```

If you want to see the verbose, then the command will be:

```python 

python3 -m unittest -v module_Test
```
By using the PyCharm, we get the below output.

<p align="center">
	<img src="pic_readme/python-unittest-example.png"
</p>

##Python Unit Test Outcome & Basic Functions

This unittest has 3 possible outcomes. They are mentioned below:

1. OK: If all test cases are passed, the output shows OK.
2. Failure: If any of test cases failed and raised an AssertionError exception
3. Error: If any exception other than AssertionError exception is raised.

There are several function under unittest module. They are listed below

<p align="center">
	<img src="pic_readme/unittest_readme2.PNG"
</p>



